<?php

namespace App;

Class Ad {
    public static $fields = [
        'title', 'description', 'keywords', 'price'
    ];
    public static $required_fields = [
        'title', 'description', 'price'
    ];
    private static $db_file = 'ads.json';

    public static function toArray($ad) {
        return (array) $ad;
    }

    public static function all() {
        return json_decode(file_get_contents(self::$db_file));
    }

    public static function find($id) {
        $ads_by_id = array_filter(self::all(), function($ad) use($id) {
            return $ad->id == $id;
        });
        return $ads_by_id ? reset($ads_by_id) : false;
    }

    public static function create($data) {
        if (!self::isValid($data)) {
            return false;
        }

        $data["id"] = uniqid();
        $ads = self::all();
        $ads[] = $data;
        self::update_file($ads);
        return true;
    }

    public static function delete($ad) {
        $ads = self::all();
        $ad_key = array_search($ad, $ads);
        unset($ads[$ad_key]);
        self::update_file($ads);
    }

    public static function update($ad, $data) {
        if (!self::isvalid($data)) {
            return false;
        }

        $updated_ad = array_merge(Ad::toArray($ad), $data);

        $ads = self::all();
        $ad_key = array_search($ad, $ads);
        $ads[$ad_key] = $updated_ad;
        self::update_file($ads);

        return $updated_ad;
    }

    public static function isValid($data) {
        foreach($data as $k => $_v) {
            if (!in_array($k, self::$fields)) {
                return false;
            }
        }

        return true;
    }

    public static function last() {
        $ads = self::all();
        return end($ads);
    }

    private static function update_file($ads) {
        file_put_contents(self::$db_file, json_encode($ads));
    }
}
