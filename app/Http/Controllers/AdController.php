<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ad;

class AdController extends Controller {

    public function index() {
        return Ad::all();
    }

    public function store(Request $request) {
        $body = $request->json()->all();

        foreach(Ad::$required_fields as $field) {
            if (!array_key_exists($field, $body)) {
                return response(["error" => "missing field '$field'"], 400);
            }
        }

        return Ad::create($body) ? response(Ad::toArray(Ad::last()), 201)
                                 : $this->invalidInputResponse();
    }

    public function show($id) {
        $ad = Ad::find($id);
        return $ad ? Ad::toArray($ad) : abort(404);
    }

    public function update(Request $request, $id) {
        $ad = Ad::find($id);
        if (!$ad) {
            abort(404);
        }

        $updated_ad = Ad::update($ad, $request->json()->all());
        return $updated_ad ? Ad::toArray($updated_ad)
                           : self::invalidInputResponse();
    }

    public function destroy($id) {
        $ad = Ad::find($id);
        if (!$ad) {
            abort(404);
        }

        return Ad::delete($ad);
    }

    private static function invalidInputResponse() {
        return response(["error" => "Invalid input"], 400);
    }

}
