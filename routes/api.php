<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('ads')->group(function() {
    Route::get('/', 'AdController@index');
    Route::post('/', 'AdController@store');
    Route::get('{id}', 'AdController@show');
    Route::delete('{id}', 'AdController@destroy');
    Route::match(['put', 'patch'], '{id}', 'AdController@update');
});
